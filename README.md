# Information Retrieval - Spark Exercise

This is the base project for our **Information Retrieval - Spark Exercise**. 
In the following we cover the basic steps to get either [IntelliJ IDEA](https://www.jetbrains.com/idea/) or [Scala IDE](http://scala-ide.org/) running, and how to import this project in order to play with [Apache Spark](https://spark.apache.org/) right away. 

If you encounter errors in the process have a look towards the end of this document.

## IntelliJ IDEA
Download and install [IntelliJ IDEA](https://www.jetbrains.com/idea/download/).

Install the Scala Plugin:

* Go to Preferences in IntelliJ
* Select Plugins
* Install the Scala Plugin (if it isn't already installed)

### Clone project
Next we clone the project which you will base your work on.

* File > New > Project from Version Control > Git
* Enter URL: https://bitbucket.org/dmir_wue/teaching-ir-spark-exercise.git
* Follow dialog
* Right-click on the project
* Maven > Reload project

### Run Example
We prepared a small example for you to get started with Spark on your local machine.

* Right-click on "SimpleSparkExample" in the package de.uniwue.ir.example
* Run 'SimpleSparkExample'

Note: It's possible that this fails with `Error: scalac: bad option: '-make:transitive'`. See the FAQ section for how to fix it.

## Scala IDE
Download and install the [Scala IDE](http://scala-ide.org/).

### Clone project
Next we clone the project which you will base your work on.

* File > Import
* Select "Projects from Git"
* Select "Clone URI"
* Enter URI: https://bitbucket.org/dmir_wue/teaching-ir-spark-exercise.git
* Follow dialog
* Rename project `teaching-ir-spark-exercise` to `ir-spark-exercise`

### Run Example
We prepared a small example for you to get started with Spark on your local machine.

* Right-click on "SimpleSparkExample.scala"
* Run As... > Scala Application

## Contact
Feel free to contact me if you have trouble setting up your project, or to submit pull requests if you ideas on how to improve this base project.

* Jan Pfister, [pfister@informatik.uni-wuerzburg.de](mailto:pfister@informatik.uni-wuerzburg.de)

## FAQ

### java.io.IOException: Could not locate executable null\bin\winutils.exe in the Hadoop binaries.
If you are a Windows user, chances are high, that you will encounter the error:

    java.io.IOException: Could not locate executable null\bin\winutils.exe in the Hadoop binaries.

To solve this, add the following line as the first line to your "main" method (adjust the path according to you setup):

    System.setProperty("hadoop.home.dir", "C:\\<PATH TO THE WORKSPACE>\\teaching-ir-spark-exercise\\assets\\winutils\\<WINDOWS VERSION>");

### Exception in thread "main" java.lang.UnsatisfiedLinkError: 'boolean org.apache.hadoop.io.nativeio.NativeIO$Windows.access0(java.lang.String, int)'
This is another issues that Windows users might encounter. To fix this, copy the file `assets/winutils/win10/bin/hadoop.dll` to `C:\Windows\System32`.  

### [Spark 2.4.x] Exception in thread "main": java.lang.ExceptionInInitializerError at org.apache.hadoop.util.StringUtils.<clinit>(StringUtils.java:79)...
If you encounter this bug, you are probably running into [this](https://stackoverflow.com/questions/58668168/how-to-fix-exception-in-thread-main-java-lang-exceptionininitializererror/58668382#58668382) problem.
In short: Spark comes with Hadoop jars (for example: Spark 2.4.5 comes with Hadoop-client 2.6.5) which can crash due to a bug when it wants to find out which Java version you are running. This bug is fixed in Hadoop versions >= 2.7.4.

You can fix the problem by overwriting the older Hadoop-jars (which come with Spark) with newer ones.
To do this, uncomment the dependency for the hadoop-client (org.apache.hadoop) in the `pom.xml` file.
Thereafter, reimport the Maven packages (In IntelliJ: right-click on the project: Maven > Reimport).

Windows users should also update the `winutils.exe` in the `assets`-directory to the version specified in the `pom.xml` for the hadoop-client package (e.g. 2.7.7).
You can download the `winutils.exe` for various Hadoop versions from [here](https://github.com/cdarlint/winutils). 

### Error: scalac: bad option: '-make:transitive'
This bug can occur when using IntelliJ.
To fix it, open the file `scala_compiler.xml` in the `.idea` folder and delete the line `<parameter value="-make:transitive" />`.
You should be able to run Scala code thereafter.

### Scala IDE issues (memory, etc.)
If you run into problems with the Scala IDE, please have a look at their [FAQ](http://scala-ide.org/docs/current-user-doc/faq/index.html).
Among other things it shows how to increase the memory used by the Scala IDE which can improve performance.
